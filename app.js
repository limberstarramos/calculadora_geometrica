const app = new Vue({
    el: '#app',
    data: {
        valor1:0,
        // area:0,
        // volumen:0,

        valor2:0,
        valor3:0,

        area1:0,
        area2:0,
        area3:0,
        area4:0,
        area5:0,
        area6:0,
        volumen1:0,
        volumen2:0,
        volumen3:0,
        volumen4:0,
        volumen5:0,
        volumen6:0,


    },
    methods:{
        calcularcubo:function(){
            this.area1=6* Math.pow(this.valor1,2)
            this.volumen1=Math.pow(this.valor1,3)

        },
        calcularparalelepipedo:function(){
        let a=this.valor1;
        let b=this.valor2;
        let c=this.valor3;
         let re= 2*((a*b)+(a*c)+(b*c));
         this.area2=re;
         this.volumen2=(a*b*c);
        },
        calcularpiramide:function(){
          this.area3=this.valor1+this.valor2;
         let vol=this.valor1*this.valor3;
          this.volumen3=((1/3)*(vol));
        },
        calcularcilindro:function(){
            let sum=this.valor1+this.valor2;
            // let ar=((2)*(Math.PI)*(this.valor2)*(sum));
            this.area4=((2)*(Math.PI)*(this.valor2)*(sum));

            let ra=Math.pow(this.valor2,2);
            this.volumen4 = ((Math.PI*(ra))*(this.valor1));
         
        },
        calcularcono:function(){
            let r=Math.pow(this.valor2,2);
            this.area5=(Math.PI*(r)+Math.PI*(this.valor2)*(this.valor3));

            this.volumen5=((Math.PI*(r)*(this.valor1) )/3);

        },
        calcularesfera:function(){
            let ra=Math.pow(this.valor1,2);
            this.area6= (4 *(Math.PI*ra));
            
            let vo=Math.pow(this.valor1,3);
            this.volumen6 =((4/3)*(Math.PI*vo));
        }

    }

})